/* Step 1: add packages */
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author talha
 */
public class ABGui extends Gui {
    
    public ABGui() {
        
        super();
        // Step 4: Give a layout
        c.setLayout(new BorderLayout()); 
        // Step 5: Create and add components
        JLabel jlbl = new JLabel("My address book");
        JTabbedPane jtab = new JTabbedPane();
        
        // Add first tab
        JPanel tab1panel = new JPanel(new GridLayout(4,2));
        // add form fields for add tab
        JLabel lblName = new JLabel("Name");
        JTextField txtName = new JTextField(10);
        
        JLabel lblAddress = new JLabel("Address");
        JTextField txtAddress = new JTextField(10);
        
        JLabel lblPhone = new JLabel("Phone");
        JTextField txtPhone = new JTextField(10);
        
        JButton savePerson = new JButton("Save");
        
        tab1panel.add(lblName);tab1panel.add(txtName);
        tab1panel.add(lblAddress);tab1panel.add(txtAddress);
        tab1panel.add(lblPhone);tab1panel.add(txtPhone);
        tab1panel.add(new JLabel(""));tab1panel.add(savePerson);
        
        jtab.addTab("ADD", tab1panel);
        
        // Add second tab
        JPanel tab2panel = new JPanel(new GridLayout(2,2));
        JLabel lblSearch = new JLabel("Search");
        JTextField txtSearch = new JTextField(10);
        
        JButton searchPerson = new JButton("Search");
        tab2panel.add(lblSearch);tab2panel.add(txtSearch);
        tab2panel.add(new JLabel(""));tab2panel.add(searchPerson);
        jtab.addTab("Search", tab2panel);
        
        c.add(jlbl, BorderLayout.NORTH);
        c.add(jtab, BorderLayout.CENTER);
        // Step 6: Resize and show frame
        showForm();
        
    }
    
    
}
