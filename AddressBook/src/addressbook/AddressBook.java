import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author talha
 */
public class AddressBook {
    
    ArrayList al;
    ABGui gui;
    
    AddressBook() {
        al = new ArrayList();
        loadAddressBook();
        gui = new ABGui();
    }
    
    public void addPerson() {
        
        String n = JOptionPane.showInputDialog("Enter name of person");
        String a = JOptionPane.showInputDialog("Enter address of person");
        String p = JOptionPane.showInputDialog("Enter phone of person");
        Person per = new Person(n,a,p);
        al.add(per);
        
    }
    
    public void search() {
        
        String n = JOptionPane.showInputDialog("Enter name of person");
        
        for(int i=0; i < al.size(); i++) {
            Person p = (Person) al.get(i);
            if(n.equals(p.name)) {
                p.print();
            }
        }
        
    }
    
    public void remove() {
        
        String n = JOptionPane.showInputDialog("Enter name of person");
        
        for(int i=0; i < al.size(); i++) {
            Person p = (Person) al.get(i);
            if(n.equals(p.name)) {
                al.remove(i);
            }
        }
    }
    
    public void saveAddressBook() {
        
        try {
            FileWriter fw = new FileWriter("addressbook.txt");
            PrintWriter pw = new PrintWriter(fw);
        
            String line;
            // write ArrayList to file
            for(int i=0; i < al.size(); i++) {
                Person p = (Person) al.get(i);
                line = p.name + "," + p.address + "," + p.phone;
                pw.println(line);
            }
            pw.flush();
            pw.close();
            fw.close();
            
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        
    }
    
    public void loadAddressBook() {
        
        String line;
        String data[];
        Person p;
        
        try {
            FileReader fr = new FileReader("addressbook.txt");
            BufferedReader br = new BufferedReader(fr);
            
            // Read line by line
            line = br.readLine();
            while(line != null) {
                // separate into name address phone
                data = line.split(",");
                // create Person objects
                p = new Person(data[0], data[1], data[2]);
                al.add(p);
                line = br.readLine();
            }
            br.close();
            fr.close();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
}
