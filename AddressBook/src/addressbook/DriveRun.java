import javax.swing.*;

/**
 *
 * @author talha
 */
public class DriveRun {
    
    public static void main(String[] args) {
        
        /* Test run for address book */
        AddressBook ab = new AddressBook();
        
        boolean test = false;
        
        while (test) {
            String choice = JOptionPane.showInputDialog("Enter choice \n Enter 1 to add \n Enter 2 to remove \n Enter 3 to search \n Enter 4 to exit");
            
            char ch = choice.charAt(0);
            
            switch (ch) {
                case '1':
                    ab.addPerson();
                    break;
                case '2':
                    ab.remove();
                    break;
                case '3':
                    ab.search();
                    break;
                case '4':
                    ab.saveAddressBook();
                    System.exit(0);
            }
        }
        
        
        
    }
}
