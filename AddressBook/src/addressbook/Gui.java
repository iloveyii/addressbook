/* Step 1: add packages */
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author talha
 */
public abstract class Gui {
    
    public JFrame myFrame;
    protected Container c;
    
    public Gui() {
        // Step 2: Create frame
        myFrame = new JFrame();
       
        // Step 3: Get the component area
        c = myFrame.getContentPane(); 
    }
    
    protected void showForm() {
        
        // Step 6: Resize and show frame
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Show in the middle of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        myFrame.setSize(600,300);
        myFrame.setLocation(dim.width/2-myFrame.getSize().width/2, dim.height/2-myFrame.getSize().height/2);
        myFrame.setVisible(true);
        
    }
    
}
