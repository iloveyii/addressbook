import javax.swing.*;

/**
 *
 * @author talha
 */
public class Person {
    public String name;
    public String address;
    public String phone;
    
    Person(String n, String a, String p) {
        name = n; 
        address = a;
        phone = p;
    }
    
    public void print() {
        JOptionPane.showMessageDialog(null, "name: " + name + "\naddress: " + address + "\nphone: " + phone);
    }
}
